import bot.commands.Command;
import bot.settings.Settings;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dv8tion.jda.api.Permission;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class SettingsDeserializerTest {

    @Test
    void deserialize() throws JsonProcessingException {
        String json1 = "{\n" +
                "  \"startDate\" : \"2021-02-24\",\n" +
                "  \"discordToken\" : null,\n" +
                "  \"superRoleIDs\" : [ \"307670315973214210\", \"292834813210001408\" ],\n" +
                "  \"messageConfig\" : {\n" +
                "    \"messageTitle\" : \"SheepleBot\",\n" +
                "    \"messageAuthor\" : \"Sheep it Render Farm\",\n" +
                "    \"messageURL\" : \"https://sheepit-renderfarm.com\",\n" +
                "    \"messageIconURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageThumbnailURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageColor\" : \"#e06d58\"\n" +
                "  },\n" +
                "  \"commands\" : [ {\n" +
                "    \"name\" : \"!pi-lorem\",\n" +
                "    \"output\" : \"Ipsum\",\n" +
                "    \"requiredPermission\" : \"MESSAGE_READ\"\n" +
                "  }, {\n" +
                "    \"name\" : \"!pi-test\",\n" +
                "    \"output\" : \"This is a test\",\n" +
                "    \"requiredPermission\" : \"MANAGE_CHANNEL\"\n" +
                "  } ]\n" +
                "}";

        String json2 = "{\n" +
                "  \"startDate\" : \"2021-02-24\",\n" +
                "  \"discordToken\" : null,\n" +
                "  \"messageConfig\" : {\n" +
                "    \"messageTitle\" : \"SheepleBot\",\n" +
                "    \"messageAuthor\" : \"Sheep it Render Farm\",\n" +
                "    \"messageURL\" : \"https://sheepit-renderfarm.com\",\n" +
                "    \"messageIconURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageThumbnailURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageColor\" : \"#e06d58\"\n" +
                "  },\n" +
                "  \"commands\" : [ {\n" +
                "    \"name\" : \"!pi-lorem\",\n" +
                "    \"output\" : \"Ipsum\",\n" +
                "    \"requiredPermission\" : \"MESSAGE_READ\"\n" +
                "  }, {\n" +
                "    \"name\" : \"!pi-test\",\n" +
                "    \"output\" : \"This is a test\",\n" +
                "    \"requiredPermission\" : \"MANAGE_CHANNEL\"\n" +
                "  } ]\n" +
                "}";

        String json3 = "{\n" +
                "  \"startDate\" : \"2021-02-24\",\n" +
                "  \"discordToken\" : null,\n" +
                "  \"superRoleIDs\" : [ \"1234\" ],\n" +
                "  \"messageConfig\" : {\n" +
                "    \"messageTitle\" : \"SheepleBot\",\n" +
                "    \"messageAuthor\" : \"Sheep it Render Farm\",\n" +
                "    \"messageURL\" : \"https://sheepit-renderfarm.com\",\n" +
                "    \"messageIconURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageThumbnailURL\" : \"https://sheepit-renderfarm.com/media/image/title.png\",\n" +
                "    \"messageColor\" : \"#e06d58\"\n" +
                "  },\n" +
                "  \"commands\" : [ {\n" +
                "    \"name\" : \"!pi-lorem\",\n" +
                "    \"output\" : \"Ipsum\",\n" +
                "    \"requiredPermission\" : \"MESSAGE_READ\"\n" +
                "  }, {\n" +
                "    \"name\" : \"!pi-test\",\n" +
                "    \"output\" : \"This is a test\",\n" +
                "    \"requiredPermission\" : \"MANAGE_CHANNEL\"\n" +
                "  } ]\n" +
                "}";

        Settings settings = new ObjectMapper().readValue(json1, Settings.class);
        assertEquals(LocalDate.parse("2021-02-24"), settings.getStartDate());
        assertEquals(2,settings.getCommands().size());
        assertTrue(settings.getCommands().containsKey("!pi-test"));
        assertTrue(settings.getCommands().containsKey("!pi-lorem"));
        Command command = settings.getCommands().get("!pi-test");
        assertEquals("!pi-test", command.getName());
        assertEquals("This is a test", command.getOutput());
        assertEquals(command.getRequiredPermission(), Permission.valueOf("MANAGE_CHANNEL"));
        assertTrue(settings.getSuperRoleIDs().size() == 2);
        assertNotNull(settings.getMessageTitle());


        settings = new ObjectMapper().readValue(json2, Settings.class);
        assertEquals(2, settings.getSuperRoleIDs().size());

        settings = new ObjectMapper().readValue(json3, Settings.class);
        assertEquals(1, settings.getSuperRoleIDs().size());
        assertTrue(settings.getSuperRoleIDs().get(0).equals("1234"));
//        System.out.println(json);
    }
}