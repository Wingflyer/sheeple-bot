package bot.commands;

import lombok.Getter;

public enum CommandCategories {
    PI("Pi-Counter"), MISC("Misc"), SERVER_MANAGEMENT("Server Management"),
    BOT_MANAGEMENT("Bot Management");

    @Getter
    private String categoryName;

    CommandCategories(String categoryName) {
        this.categoryName = categoryName;
    }
}
