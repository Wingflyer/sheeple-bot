package bot;

import bot.settings.Settings;
import lombok.NonNull;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JoinListener extends ListenerAdapter {
    private Timer kickTimer; //kick new members after 15 minutes if they haven't registered
    private StatisticsCollector statisticsCollector;

    public JoinListener(StatisticsCollector statisticsCollector) {
        this.kickTimer = new Timer(true);
        this.statisticsCollector = statisticsCollector;
    }


    @Override
    public void onGuildMemberJoin(@NonNull GuildMemberJoinEvent event) {
        if(!event.getGuild().getId().equals(Settings.getInstance().getGuildID())) {
            return;
        }

        statisticsCollector.setNewlyJoinedMembers(statisticsCollector.getNewlyJoinedMembers() + 1);
        long gracePeriod = Settings.getInstance().getKickUnregisteredMembersAfter_ms();
        Logger logger = Logger.getLogger(Settings.LOGGER_NAME);

        Member newMember = event.getMember();
        Guild guild = event.getJDA().getGuildById(Settings.getInstance().getGuildID());
        if (guild == null) {
            return;
        }
        Role memberRole = guild.getRoleById(Settings.getInstance().getMemberRoleID());
        if (memberRole == null) {
            logger.log(Level.SEVERE, () -> String.format("Member role not found. ID: %s. Auto-Kick disabled", Settings.getInstance().getMemberRoleID()));
            event.getJDA().removeEventListener(this);
            return;
        }
        try {
            TimerTask task = createKickTask(guild, newMember, memberRole);
            kickTimer.schedule(task, gracePeriod);
        }
        catch (NullPointerException e) {
            logger.log(Level.WARNING, e, () -> String.format("Could not create kick task for newly joined member %s", newMember.getUser().getAsTag()));
        }
    }

    private TimerTask createKickTask(@NonNull Guild guild, @NonNull Member newMember, @NonNull Role memberRole) {
        Logger logger = Logger.getLogger(Settings.LOGGER_NAME);
        String kickReason = Settings.getInstance().getKickReason();

        return new TimerTask() {
            @Override
            public void run() {
                guild.retrieveMemberById(newMember.getId()).queue(m -> {
                    if (m != null && !m.getRoles().contains(memberRole)) {
                        try {
                            guild.kick(newMember, kickReason).queue();
                            statisticsCollector.setAutoKickedMembers(statisticsCollector.getAutoKickedMembers() + 1);
                            logger.log(Level.INFO, () -> String.format("Kicked %s: %s", newMember.getUser().getAsTag(), kickReason));
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, e, () -> String.format("Exception on trying to kick member %s (ID: %d): ", newMember.getUser().getAsTag(), newMember.getIdLong()));
                        }
                    }
                });
            }
        };
    }
}
