package bot.graphing;

import java.awt.*;
import java.util.Comparator;
import java.util.List;

public class Graph implements Comparable<Graph> {

    private String name;
    private Color color;
    private RolloverIntegerArray values;

    public Graph (String name, Color color, RolloverIntegerArray values) {
        this.name = name;
        this.color = color;
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public List<Integer> getValues() {
        return values.getAsList();
    }

    public Integer getNewestValue() {
        return values.get();
    }

    public void setNewestValue(Integer value) {
        values.set(value);
    }

    public void setValues(List<Integer> values) {
        this.values = new RolloverIntegerArray(values.size());
        for (var value : values) {
            this.values.set(value);
            this.values.rollover();
        }
    }

    /**
     * @see {@link RolloverIntegerArray#rollover()}
     */
    public void rollover() {
        this.values.rollover();
    }

    @Override
    public int compareTo(Graph graph) {
        return values.getAsList().stream().max(Comparator.naturalOrder()).orElse(-1)
                .compareTo(graph.values.getAsList().stream().max(Comparator.naturalOrder()).orElse(-1));
    }
}
