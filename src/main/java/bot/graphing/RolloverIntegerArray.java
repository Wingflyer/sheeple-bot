package bot.graphing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RolloverIntegerArray {

    private int index;

    private Integer[] array;


    public RolloverIntegerArray(int capacity) {
        array = new Integer[capacity];
        Arrays.fill(array, 0);
        index = 0;
    }

    public void set(Integer value) {
        array[index] = value;
    }

    public Integer get() {
        return array[index];
    }

    /**
     * Moves the pointer for insertion forwards, preserving old values. The amount of retained values is equal to this structures capacity
     */
    public void rollover() {
        index = (index + 1) % array.length;
        array[index] = 0;
    }

    public List<Integer> getAsList() {
        List<Integer> list = new ArrayList<>(array.length);

        int tmpIndex = (index + 1) % array.length;
        for (int i = 0; i < array.length; i++) {
            list.add(array[tmpIndex]);
            tmpIndex = (tmpIndex + 1) % array.length;
        }
        return list;
    }

}
