package bot;

import bot.settings.Settings;
import lombok.NonNull;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterButtonInteractionListener extends ListenerAdapter {

    private ExecutorService executor;
    private String buttonID;
    private StatisticsCollector statisticsCollector;

    public RegisterButtonInteractionListener(String buttonID, StatisticsCollector statisticsCollector) {
        executor = Executors.newCachedThreadPool();
        this.buttonID = buttonID;
        this.statisticsCollector = statisticsCollector;
    }

    @Override
    public void onButtonInteraction(@NotNull ButtonInteractionEvent event) {
        String componentID = event.getComponentId();
        if (componentID.equals(buttonID)) {
            if (Settings.getInstance().getIgnoredMembers().containsKey(event.getMember().getId())) {
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO, () -> String.format("%s tried to register but was ignored", event.getMember().getUser().getAsTag()));
                event.replyEmbeds(Settings.getInstance().createMessage("Request ignored")).setEphemeral(true).queue();
                return;
            }

            Member member = event.getMember();
            Role memberRole = event.getGuild().getRoleById(Settings.getInstance().getMemberRoleID());
            if(memberRole == null) {
                event.getHook().sendMessageEmbeds(Settings.getInstance().createMessage("Couldn't find the member role")).queue();
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, () -> String.format("Failed to register %s : Could not find member role %s", member, Settings.getInstance().getMemberRoleID()));
                return;
            }
            if(member == null) {
                event.getHook().sendMessageEmbeds(Settings.getInstance().createMessage("Couldn't find the member")).queue();
                return;
            }

            register(event.getGuild(), member, memberRole);
            event.replyEmbeds(Settings.getInstance().createMessage("Welcome!")).setEphemeral(true).queue();
        }
    }

    private void register(@NonNull Guild guild, @NonNull Member member, @NonNull Role role) {
        try {
            guild.addRoleToMember(member, role).queue();
            statisticsCollector.setNewlyRegisteredMembers(statisticsCollector.getNewlyRegisteredMembers() + 1);
        } catch (Exception e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, e, () -> "Exception on trying to register " + member);
        }
    }
}